#Вправа 1
def is_palindrome(word):
    length = len(word)
    for i in range(length // 2):
        if word[i] != word[length - 1 - i]:
            return False
    return True
print(is_palindrome("panda"))

def is_palindrome(word):
    return word == word[::-1]
print(is_palindrome("ada"))

#Вправа 2
def caesar_encrypt(text, shift):
    encrypted_text = ""
    for char in text:
        if char.isalpha():
            if char.isupper():
                encrypted_text += chr((ord(char) - ord('A') + shift) % 26 + ord('A'))
            else:
                encrypted_text += chr((ord(char) - ord('a') + shift) % 26 + ord('a'))
        else:
            encrypted_text += char
    return encrypted_text

def caesar_decrypt(encrypted_text, shift):
    decrypted_text = ""
    for char in encrypted_text:
        if char.isalpha():
            if char.isupper():
                decrypted_text += chr((ord(char) - ord('A') - shift) % 26 + ord('A'))
            else:
                decrypted_text += chr((ord(char) - ord('a') - shift) % 26 + ord('a'))
        else:
            decrypted_text += char
    return decrypted_text
    
    
text = "I want a vacation and a cake."
shift = 7

encrypted_text = caesar_encrypt(text, shift)
print(encrypted_text)  # Вивід зашифрованого тексту

decrypted_text = caesar_decrypt(encrypted_text, shift)
print(decrypted_text)  # Вивід розшифрованого тексту





