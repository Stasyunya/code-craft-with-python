#Вправа 1(1)
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Circle:
    def __init__(self, center, radius):
        self.center = center
        self.radius = radius


center = Point(150, 100)
radius = 75
circle = Circle(center, radius)


print("Center: ({}, {})".format(circle.center.x, circle.center.y))
print("Radius:", circle.radius)


#Вправа 1(2)

import math

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Circle:
    def __init__(self, center, radius):
        self.center = center
        self.radius = radius

def point_in_circle(circle, point):
    distance = math.sqrt((point.x - circle.center.x) ** 2 + (point.y - circle.center.y) ** 2)
    return distance <= circle.radius


center = Point(150, 100)
radius = 75
circle = Circle(center, radius)


point = Point(160, 110)
print(point_in_circle(circle, point))



#Вправа 1(3)

import math

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Circle:
    def __init__(self, center, radius):
        self.center = center
        self.radius = radius

class Rectangle:
    def __init__(self, top_left, width, height):
        self.top_left = top_left
        self.width = width
        self.height = height

def rect_in_circle(circle, rectangle):

    top_right = Point(rectangle.top_left.x + rectangle.width, rectangle.top_left.y)
    bottom_left = Point(rectangle.top_left.x, rectangle.top_left.y - rectangle.height)
    bottom_right = Point(rectangle.top_left.x + rectangle.width, rectangle.top_left.y - rectangle.height)

  
    if point_in_circle(circle, rectangle.top_left) and \
            point_in_circle(circle, top_right) and \
            point_in_circle(circle, bottom_left) and \
            point_in_circle(circle, bottom_right):
        return True

    return False

def point_in_circle(circle, point):
    distance = math.sqrt((point.x - circle.center.x) ** 2 + (point.y - circle.center.y) ** 2)
    return distance <= circle.radius


center = Point(150, 100)
radius = 75
circle = Circle(center, radius)


top_left = Point(130, 80)
width = 40
height = 30
rectangle = Rectangle(top_left, width, height)


print(rect_in_circle(circle, rectangle))


#Вправа 1(4)

import math

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Circle:
    def __init__(self, center, radius):
        self.center = center
        self.radius = radius

class Rectangle:
    def __init__(self, top_left, width, height):
        self.top_left = top_left
        self.width = width
        self.height = height

def rect_circle_overlap(circle, rectangle):
    
    top_right = Point(rectangle.top_left.x + rectangle.width, rectangle.top_left.y)
    bottom_left = Point(rectangle.top_left.x, rectangle.top_left.y - rectangle.height)
    bottom_right = Point(rectangle.top_left.x + rectangle.width, rectangle.top_left.y - rectangle.height)

   
    if point_in_circle(circle, rectangle.top_left) or \
            point_in_circle(circle, top_right) or \
            point_in_circle(circle, bottom_left) or \
            point_in_circle(circle, bottom_right):
        return True

    return False

def point_in_circle(circle, point):
    distance = math.sqrt((point.x - circle.center.x) ** 2 + (point.y - circle.center.y) ** 2)
    return distance <= circle.radius


center = Point(150, 100)
radius = 75
circle = Circle(center, radius)


top_left = Point(130, 80)
width = 40
height = 30
rectangle = Rectangle(top_left, width, height)


print(rect_circle_overlap(circle, rectangle))


#Вправа 2

class Time:
    def __init__(self, hours, minutes, seconds):
        self.hours = hours
        self.minutes = minutes
        self.seconds = seconds

    def __str__(self):
        return f"{self.hours:02d}:{self.minutes:02d}:{self.seconds:02d}"

    def __add__(self, other):
        total_seconds = self._to_seconds() + other._to_seconds()
        return self._from_seconds(total_seconds)

    def __le__(self, other):
        return self._to_seconds() <= other._to_seconds()

    def __eq__(self, other):
        return self._to_seconds() == other._to_seconds()

    def __ge__(self, other):
        return self._to_seconds() >= other._to_seconds()

    def _to_seconds(self):
        return self.hours * 3600 + self.minutes * 60 + self.seconds

    @staticmethod
    def _from_seconds(seconds):
        hours = seconds // 3600
        minutes = (seconds % 3600) // 60
        seconds = seconds % 60
        return Time(hours, minutes, seconds)


time1 = Time(2, 30, 45)
time2 = Time(1, 45, 20)

print(time1)
print(time2)

time_sum = time1 + time2
print(time_sum)

print(time1 <= time2)
print(time1 == time2)
print(time1 >= time2)


