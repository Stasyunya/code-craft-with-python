#Вправа 1
def is_triangle():
    a = int(2)   #вручну закладаємо значення
    b = int(3)
    c = int(4)
    
    if a + b > c and a + c > b and b + c > a:
        print("Yes")
    else:
        print("No")
is_triangle()

#Вправа 2
def is_triangle():
    a = int(input("Введіть першу довжину відрізка: "))
    b = int(input("Введіть другу довжину відрізка: "))
    c = int(input("Введіть третю довжину відрізка: "))

    if a + b > c and a + c > b and b + c > a:
        print("Yes")
    else:
        print("No")
is_triangle()

#Вправа 3
def print_string(string, count):
    for _ in range(count):
        print(string)
print_string('Hello, World!', 3)

#Вправа 4
def recurse(n, s):
    if n == 0:
        print(s)
    else:
        recurse(n-1, n+s)

recurse(3, 0)
# Дасть у результаті 6. Покроково (3, 0) (2, 3) (1, 5) (0, 6)

Що станеться, якщо викликати цю функцію так: recurse(-1, 0)?
# Програма виконає багато кроків, а потім видать RecursionError

#Вправа 5
def is_power_of_two(n):
    if n <= 0:
        return False
    
    while n > 1:
        if n % 2 != 0:
            return False
        n = n // 2
    
    return True
print(is_power_of_two(12))   #Задаємо число, яке хочемо перевірити

#Вправа 6
def is_prime(n, divisor=2):
    if n <= 1:
        return False
    
    if n == 2:
        return True
    
    if n % divisor == 0:
        return False
    
    if divisor * divisor > n:
        return True
    
    return is_prime(n, divisor + 1)
    
print(is_prime(13))






