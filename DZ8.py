#Вправа 1
with open('input.txt', 'w') as file:
    file.write('Hello, world!')
    
def sed(word1, word2, inputFile, outputFile):
    with open(inputFile, 'r') as file:
        content = file.read()

    modified_content = content.replace(word1, word2)

    with open(outputFile, 'w') as file:
        file.write(modified_content) 
        
    with open(outputFile, 'r') as file:
        resultContent = file.read()
    print(resultContent)    
        
sed('ll', 'rr', 'input.txt', 'output.txt')







