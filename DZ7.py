#Вправа 1
def cumsum(numbers):
    cumulative_sum = []
    current_sum = 0
    for num in numbers:
        current_sum += num
        cumulative_sum.append(current_sum)
    return cumulative_sum
numbers = [2, 7, 8, 15, 25] # Вхідний список
result = cumsum(numbers)
print(result)

#Вправа 2
def is_sorted(lst):
    return all(lst[i] <= lst[i+1] for i in range(len(lst)-1))
lst = [1, 2, 3, 4, 5]
print(is_sorted(lst)) 

#Вправа 3
def in_bisect(sorted_list, target):
    left = 0
    right = len(sorted_list) - 1

    while left <= right:
        mid = (left + right) // 2
        if sorted_list[mid] == target:
            return True
        elif sorted_list[mid] < target:
            left = mid + 1
        else:
            right = mid - 1

    return False
sorted_list = ['absolute', 'accept', 'account', 'accountant', 'achieve', 'acquire', 'act']
target = 'act'
print(in_bisect(sorted_list, target))











